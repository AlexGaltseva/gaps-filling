function [ matrix ] = gaps_blanking( matrix )
c = find(isnan(matrix(:,:)));
for i = 1:length(c)
    tmp = (matrix(c(i)-1) + matrix(c(i)+1))/2;
    matrix(c(i)) = tmp;
end
end

