function [ timeSeries ] = interpolationFilling( timeSeries, params )
if ~isfield(params, 'NoRowsWithAddedGaps') || ... % ������������ �� ������!
   ~isfield(params, 'x') 

    error('Some of obligatory params are absent');
end

for i = params.NoRowsWithAddedGaps
    timeSeries(i,:) = interp1(timeSeries(i,:), params.x, 'spline'); 
end

end
