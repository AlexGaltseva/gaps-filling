function [ quantiles, sum_mse ] = getQantiles( timeSeries, params )
if  ~isfield(params, 'nGapsInRowArr') || ...
    ~isfield(params, 'NoRowsWithAddedGaps')||...
    ~isfield(params, 'nIterations') ||...
    ~isfield(params, 'gapsFillingFName')

    error('Some of obligatory params are absent');
end

x = 1:length(timeSeries); 
sum_mse = zeros(length(params.nGapsInRowArr), params.nIterations);      % length(matrixShort));
quantiles = struct('min', [] , 'q25', [], 'q50', [] , 'q75', [], 'max', []); 

for nGapsInRow = params.nGapsInRowArr
    
    params_ = struct (...
        'x', x, ...
        'nGapsInRow', nGapsInRow,...
        'NoRowsWithAddedGaps', params.NoRowsWithAddedGaps);
    i = find(params.nGapsInRowArr == nGapsInRow);
    
    for j = 1:params.nIterations
        matrixWithGaps = addGaps( timeSeries, params.rndstr, params_ );
        reconstructedMatrix = feval( params.gapsFillingFName, matrixWithGaps, params_);
%         a = MSE_data(timeSeries, reconstructedMatrix)
        sum_mse(i,j) = sum(MSE_data(timeSeries, reconstructedMatrix)); % �� ���� ��� ���� ���������� �� �������� �����
    end
    
         quantiles.min(i) = min(sum_mse(i,:));
         quantiles.q25(i) = quantile(sum_mse(i,:),0.25);
         quantiles.q50(i) = quantile(sum_mse(i,:),0.5);
         quantiles.q75(i) =  quantile(sum_mse(i,:),0.75);
         quantiles.max(i) = max(sum_mse(i,:));
         
end

end

