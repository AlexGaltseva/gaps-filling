function [ nGapsInRowArr ] = returnNGapsInRowArray( params )
if ~isfield(params, 'timeSeriesLength') || ...
    ~isfield(params, 'rangeStart') || ...
    ~isfield(params, 'rangeEnd') || ...
    ~isfield(params,'rangeStep') 
error('Some of obligatory params are absent');
end

nGapsInRowArr = [];
currentValueNGaps = params.rangeStart;
i = 1;
while currentValueNGaps <= params.rangeEnd
    nGapsInRowArr(i) = params.timeSeriesLength*currentValueNGaps/100;
    currentValueNGaps = currentValueNGaps+params.rangeStep;
    i = i+1;
end
nGapsInRowArr = round(nGapsInRowArr);


end

