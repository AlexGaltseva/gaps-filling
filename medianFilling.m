function [ timeSeries ] = medianFilling( timeSeries, params )
if ~isfield(params, 'NoRowsWithAddedGaps')
    error('Some of obligatory params are absent');
end

for i = params.NoRowsWithAddedGaps
    row = timeSeries(i,:);
    row(isnan(row)) = median(row(~isnan(row)));  % mean - avarage
    timeSeries(i,:) = row;
end
end

