function [ timeSeries ] = kmeansClustering( timeSeries, params )
if ~isfield(params, 'NoRowsWithAddedGaps')

    error('Some of obligatory params are absent');
end
% if ~isfield(params, 'nClusters')
%     params.nClusters = 5;
% end

K = 16;
params_ = struct('NoRowsWithAddedGaps',params.NoRowsWithAddedGaps);
prefillReconstructedMatrix = feval( 'meanFilling', timeSeries, params_);
% [IDX, K] = best_kmeans(prefillReconstructedMatrix); % vectorOfTimeSeriesDistributionOverClusters
IDX = kmeans(prefillReconstructedMatrix, K);


for k = 1:K
    cluster = find(IDX == k);
    l = length(find(IDX == k));
    for j = 1:l
        clusterArr(k,j) = cluster(j);
    end
    
    timeSeries = gapsFillingInsideCluster( timeSeries, clusterArr(k,:));
end

% timeSeries = gapsFillingInsideCluster( timeSeries, cl1' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl2' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl3' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl4' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl5' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl6' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl7' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl8' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl9' );
% timeSeries = gapsFillingInsideCluster( timeSeries, cl10' );
% 

end

