function [ timeSeries ] = meanFilling( timeSeries, params )
if ~isfield(params, 'NoRowsWithAddedGaps')
    error('Some of obligatory params are absent');
end

for i = params.NoRowsWithAddedGaps
    row = timeSeries(i,:);
    row(isnan(row)) = mean(row(~isnan(row))); 
    timeSeries(i,:) = row;
end
end

