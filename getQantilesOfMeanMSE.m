function [ quantiles, mean_mse ] = getQantilesOfMeanMSE( timeSeries, params )
if  ~isfield(params, 'nGapsInRowArr') || ...
    ~isfield(params, 'NoRowsWithAddedGaps')||...
    ~isfield(params, 'nIterations') ||...
    ~isfield(params, 'gapsFillingFName')

    error('Some of obligatory params are absent');
end

x = 1:length(timeSeries); 
mean_mse = zeros(length(params.nGapsInRowArr), params.nIterations);      % length(matrixShort));
quantiles = struct('min', [] , 'q25', [], 'q50', [] , 'q75', [], 'max', []); 

for nGapsInRow = params.nGapsInRowArr
    
    params_ = struct (...
        'x', x, ...
        'nGapsInRow', nGapsInRow,...
        'NoRowsWithAddedGaps', params.NoRowsWithAddedGaps);
    
    i = find(params.nGapsInRowArr == nGapsInRow);
    
    for j = 1:params.nIterations
        matrixWithGaps = addGaps( timeSeries, params.rndstr, params_ );
        reconstructedMatrix = feval( params.gapsFillingFName, matrixWithGaps, params_);
        mean_mse(i,j) = mean(MSE_data(timeSeries, reconstructedMatrix)); % �� ���� ��� ���� ���������� �� �������� �����
    end
    
         quantiles.min(i) = min(mean_mse(i,:));
         quantiles.q25(i) = quantile(mean_mse(i,:),0.25);
         quantiles.q50(i) = quantile(mean_mse(i,:),0.5);
         quantiles.q75(i) =  quantile(mean_mse(i,:),0.75);
         quantiles.max(i) = max(mean_mse(i,:));
         
end

end

