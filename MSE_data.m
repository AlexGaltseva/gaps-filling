function [ MSE ] = MSE_data( DataMdl, DataOut )
MSE = sum( (DataMdl-DataOut).^2, 1 ) ./ size( DataMdl, 1 );
end 