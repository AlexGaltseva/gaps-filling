function [ table ] = returnTableRepresent( row, period )

n = length(row)-period;
table = zeros(period+1, n);

 for j = 1:n
     k = j;
     for i = 1:period+1
         table(i,j) = row(k);
         k = k+1;
     end
 end

end

