function [ input, output ] = TSPreprocessing( timeSeries, period )
m = length(timeSeries)/(period+1);
input = zeros(period, m);
k = 1;
for j = 1:fix(m)
    for i = 1:period
        input(i,j) = timeSeries (k);
        k = k+1;
    end
    output(j) = timeSeries(k);
    k = k+1;
end

end

