function [ timeSeries ] = addGaps( timeSeries, rndStream, params)
if  ~isfield(params, 'nGapsInRow') || ...
    ~isfield(params, 'NoRowsWithAddedGaps')
    error('Some of obligatory params are absent');
end

len = length(timeSeries);
for i = params.NoRowsWithAddedGaps
%     randVar = randi(rndStream, [1, len], 1, params.nGapsInRow);
    rndVar = randperm(rndStream,len,params.nGapsInRow);
    for j = 1:params.nGapsInRow
        timeSeries(i, rndVar(1, j)) = NaN;
    end
end

end


