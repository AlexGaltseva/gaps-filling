

data = readtable('data1.txt', 'delimiter', ' ');
TS = createMatrix(data);
TS = TS(:,1:121);

%%

% data = readtable('files_txt/0.txt', 'delimiter', ' ');
% TS_cur = createMatrix(data);
% for f = 1:9
%     fNum = num2str(f);
%     fDir = strcat('files_txt/', fNum);
%     fName = strcat(fDir, '.txt');
%     data = readtable(fName, 'delimiter', ' ');
%     
%     TS_next = createMatrix(data);
%     TS = [TS_cur TS_next];
%     TS_cur = TS;
% 
% end

%%
rndStream = RandStream.create('mt19937ar','NumStreams',1,'Seed', 1);
x = 1:length(TS)
NoRowsWithAddedGaps = [2 5 6 9 12:19 24 27 31:42 46:49 53 55 58:62];
%%
% row = [1 2 NaN 4 5 NaN 7 8 9 NaN];
% row = UNNFilling_v2( row )
%%
params = struct(...
    'timeSeriesLength', length(TS),...
    'rangeStart', 10,...
    'rangeEnd', 40,...
    'rangeStep', 5); 

nGapsInRowArr = returnNGapsInRowArray( params );
nIterations = 10;

%%

params = struct(...
    'gapsFillingFName', 'UNNForecasting',...
    'nGapsInRowArr', nGapsInRowArr,...
    'NoRowsWithAddedGaps', NoRowsWithAddedGaps, ...
    'nIterations', nIterations, ...   
    'rndstr', rndStream);

quantilesMF = getQantilesOfMeanMSE( TS, params );


'it is all!'

qantilesArray = [quantilesMF.min; quantilesMF.q25; quantilesMF.q50; quantilesMF.q75; quantilesMF.max]';


%%

params = struct(...
    'gapsFillingFName', 'meanFilling',...
    'nGapsInRowArr', nGapsInRowArr,...
    'NoRowsWithAddedGaps', NoRowsWithAddedGaps, ...
    'nIterations', nIterations, ...   
    'rndstr', rndstream);

quantilesMF = getQantilesOfMeanMSE( TS, params );

qantilesArray = [quantilesMF.min; quantilesMF.q25; quantilesMF.q50; quantilesMF.q75; quantilesMF.max]';

%%
params = struct(...
    'gapsFillingFName', 'kmeansClustering',...
    'nGapsInRowArr', nGapsInRowArr,...
    'NoRowsWithAddedGaps', NoRowsWithAddedGaps, ...
    'nIterations', 100, ...   
    'rndstr', rndStream);

quantilesKF = getQantilesOfMeanMSE( TS, params );

qantilesArrayCl = [quantilesKF.min; quantilesKF.q25; quantilesKF.q50; quantilesKF.q75; quantilesKF.max];
%%
% attitude = quantilesKF.q50./quantilesMF.q50

%%

% nGapsInRowArr = [51:3:151];
tmpArr1 = [quantilesKF.min; quantilesKF.max];
tmpArr2 = [quantilesKF.q25; quantilesKF.q75];

plot(nGapsInRowArr, tmpArr1,'-k.');
hold on;

plot(nGapsInRowArr, tmpArr2, '--k.');
hold on;
plot(nGapsInRowArr, quantilesKF.q50, '-k.', 'LineWidth',2);
hold on;


title('�������������� ��������� ����� ��������������');
xlabel('����� ���������, ��');
ylabel('���, ���');
%%

tmpArr1 = [quantilesMF.min];  %; quantilesMF.max];
tmpArr2 = [quantilesMF.q25; quantilesMF.q75];

plot(nGapsInRowArr, tmpArr1,'-k');
hold on;

plot(nGapsInRowArr, tmpArr2, '--k');
hold on;
plot(nGapsInRowArr, quantilesMF.q50, '-k', 'LineWidth',2);
hold on;


title('�������������� �������� ����� ����������� ������');
xlabel('����� ���������, ��');
ylabel('���, ���');


%%
% subplot(1,3,1)
% plot(qantilesArray,'DisplayName','qantilesArray');
% 
% subplot(1,3,2)
% plot(qantilesArrayCl,'DisplayName','qantilesInterpArray');
% 
% subplot(1,3,3)

%%

% params = struct(...
%     'gapsFillingFName', 'medianFilling',...
%     'nGapsInRowArr', nGapsInRowArr,...
%     'NoRowsWithAddedGaps', NoRowsWithAddedGaps, ...
%     'nIterations', nIterations, ...   
%     'rndstr', rndstream);
% 
% [quantilesMedian, mean_mse] = getQantilesOfMeanMSE( matrix, params );
% 
% qantilesMedianArray = [quantilesMedian.min; quantilesMedian.q25; quantilesMedian.q50; quantilesMedian.q75; quantilesMedian.max]';

%%
params = struct(...
    'gapsFillingFName', 'interpolationFilling',...
    'nGapsInRowArr', nGapsInRowArr,...
    'NoRowsWithAddedGaps', NoRowsWithAddedGaps, ...
    'nIterations', 100, ...   
    'rndstr', rndstream);

[quantilesIF, mean_mse] = getQantilesOfMeanMSE( TS, params );

qantilesInterpArray = [quantilesIF.min; quantilesIF.q25; quantilesIF.q50; quantilesIF.q75]';

%% 
tmpArr1 = [quantilesIF.min];
tmpArr2 = [quantilesIF.q25; quantilesIF.q75];

plot(nGapsInRowArr, tmpArr1,'-k');
hold on;

plot(nGapsInRowArr, tmpArr2, '--k');
hold on;
plot(nGapsInRowArr, quantilesIF.q50, '-k', 'LineWidth',2);
hold on;


title('�������������� ��������� ����� �����������������');
xlabel('����� ���������, ��');
ylabel('���, ���');


%%

% qantilesInterpArray = [quantilesInterp.min; quantilesInterp.q25; quantilesInterp.q50]';
    

plot(qantilesArray,'DisplayName','qantilesArray');
hold on;

plot(qantilesInterpArray,'DisplayName','qantilesInterpArray');
hold on;
%%
% M = [V0 V1 V2];
% 


%%
%rangeAtribute - �������� �����, � ������� ����� ��������� ��������
params = struct(...
    'gapsFillingFName', 'interpolation',...
    'nGapsInRow', nGapsInRowArr,...
    'rangeAtribute', 5:8, ...
    'nIteraions', 1, ...    %������� ����� �������� 50-100, ����� ������ ���������� ���� �� ������������� �� ������ ����������
    'rndstr', rndstr);
quaInterp = getQantiles( TS, params );

params2 = struct(...
    'gapsFillingFName', 'meanFilling',...
    'nGapsInRow', nGapsInRowArr,...
    'rangeAtribute', 5:8, ...
    'nIteraions', 1, ...
    'rndstr', rndstr);

quaMediums = getQantiles( TS, params2 );

elbow();

%%
gapsPoints = NGapsInRow;
figure(2);
clf;
hold on;
%plot(gapsPoints, matr(1,:), '--r');
plot(gapsPoints, quaInterp.q50, '-r.');
plot(gapsPoints, quaMediums.q50, '-k.');

legend('interpolation filling',...
       'medium filling',...
       'Location', 'northwest');
% plot(gapsPoints, matr(3,:), 'k-');
% plot(gapsPoints, matr(4,:), '-b.');
%plot(gapsPoints, matr(5,:), '--b');
hold off;
grid on;


for j = 1:4
    subplot(2,2,j)
    histfit(sum_mse(j,:))
end

% time_series = array2table(TS');
% writetable(time_series, 'time series (v2).csv');
           