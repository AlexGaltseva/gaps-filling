function [ timeSeries ] = UNNForecasting(  timeSeries, params )
if ~isfield(params, 'NoRowsWithAddedGaps')

    error('Some of obligatory params are absent');
end

for RowWithGaps = params.NoRowsWithAddedGaps
    timeSeries(RowWithGaps,:) = UNNFilling_v2(timeSeries(RowWithGaps,:));
end

end

