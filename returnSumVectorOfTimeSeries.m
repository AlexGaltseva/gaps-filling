function [ sumVector ] = returnSumVectorOfTimeSeries( timeSeries )
sizeTimeSeries = size(timeSeries);
sumVector = zeros(sizeTimeSeries(1), 1);
for i = 1 : sizeTimeSeries(1);
    sumVector(i) = sum(timeSeries(i,:));
end

end

