function [  ] = TEMP( )

% P=zeros(100,21);
% T=zeros(1,100);
% x=0:5.e-2:1;
% for i=1:100
%     c=0.9*rand+0.1;
%     a=0.9*rand+0.1;
%     s=0.9*rand+0.1; 
%     T(1,i)=c;
% %     T(2,i)=a;
% %     T(3,i)=s;
%     P(i,:)=c*exp(-((x-a).^2/s));
% end; 
% 
% P = P';
% 
% net = newff(minmax(P),[21,10,3,1],{'logsig' 'logsig' 'purelin' 'purelin'},'trainlm');
% % net.performFcn='sse';
% net.trainParam.goal=0.01;
% net.trainParam.epochs=100;
% [net , tr] = train(net,P,T);
% y=sim(net,P);
% [m,b,r]=postreg(y(1,:),T(1,:));

% P = [0 1 2 3 4 5 6 7 8 9 10];
% 
% T = [0 1 2 3 4 3 2 1 2 3 4];
% 
% net = newff([0 10],[5 1],{'tansig' 'purelin'});
P = [0 1; 1 2; 2 3; 3 4];
P = P';
T = [2 3 4 5];
vp = [4 ; 5];
vt = [NaN];
net = newff(minmax(P),[2,1],{ 'logsig' 'purelin'},'trainlm');

% Y = sim(net,P);
% plot(P,T,P,Y,'o')
% 
net.trainParam.goal=0.01;
net.trainParam.epochs=50;
[net,tr, Y]=train(net,P,T);
Y

[net1,tr1, Y1]=train(net,vp,vt);
Y1
% net.trainParam.epochs = 50;
% net = train(net,P,T);
% Z = sim(net,P);
% 
% plot(P,T,P,Z,'o')
end

