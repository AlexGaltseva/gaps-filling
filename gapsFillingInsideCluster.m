function [ timeSeries ] = gapsFillingInsideCluster( timeSeries, clusterMas )

lenClusterMas = length(find(clusterMas~=0));
gapsTable = zeros(lenClusterMas,length(timeSeries));
clusterMas = clusterMas(1:lenClusterMas);

for i = 1:lenClusterMas
    gapsTable(i,:) = isnan(timeSeries(clusterMas(i),:));
end

for i = clusterMas
    gapsNo = find(isnan(timeSeries(i,:)));
    for j = gapsNo
        isRowNan = find(gapsTable(:,j));
        if (length(isRowNan))==lenClusterMas
            line = timeSeries(i,:);
            timeSeries(i,j) = mean(line(~isnan(line)));
        else
            row = timeSeries(clusterMas,j);
            timeSeries(i,j) = mean(row(~isnan(row)));
        end
            
    end
end

end

