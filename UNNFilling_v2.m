function [ row ] = UNNFilling_v2( row )

%����� ���� ������ ��� ������� �������� �� ������+1!!!
p = 10; %period
noGapsArr = find(isnan(row));
i = 1;
 while noGapsArr(i) - p < 2
     row(noGapsArr(i)) = mean(row(~isnan(row)));
     noGapsArr = noGapsArr(2:end);
 end
 
table = returnTableRepresent(row, p); %last row it's output
 
cleared_table = table(:, 1:noGapsArr(1)-(p+1));

for i = 1:length(noGapsArr)-1
     start_section = noGapsArr(i)+1;
     end_section = noGapsArr(i+1) - (p+1); 
     if start_section < end_section
        cleared_table = [cleared_table, table(:,start_section:end_section)];
     end
end

input = cleared_table(1:end-1,:);
output = cleared_table(end,:);
net = newff(minmax(input),[p,1],{ 'logsig' 'purelin'},'trainlm');
net.trainParam.goal=0.01;
net.trainParam.epochs=50;

[net,tr]=train(net,input,output);

for noGap = noGapsArr
    input = table(1:end-1,noGap-p);
    gap = table(end,noGap-p);
    [net1,tr,Y]=train(net,input,gap);
    row(noGap) = Y;
    table = returnTableRepresent(row, p);
end
    
end

