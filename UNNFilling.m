function [ row ] = UNNFilling( row )

noGapsArr = find(isnan(row));
p = 10; %period

while noGapsArr
    k = 1;
    
    m = noGapsArr(1)-p-1;
    if m <= 0
        row(noGapsArr(1)) = mean(row(~isnan(row)));
    else
    
    input = zeros(p,m);
    for j = 1:m
        for i = 1:p
            input(i,j) = row(k);
            k = k+1;
        end
        output(j) = row(k);
        k = k-p+1;
    end 
    net = newff(minmax(input),[p,1],{ 'logsig' 'purelin'},'trainlm');
    net.trainParam.goal=0.01;
    net.trainParam.epochs=50;
    [net,tr]=train(net,input,output);
    vp = row(k:k+p-1);
    gap = row(k+p);
    [net,tr,Y]=train(net,vp',gap);
    row(k+p) = Y;
    end
    noGapsArr = noGapsArr(2:end);
   
end

end

