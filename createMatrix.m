function [ matrix ] = createMatrix( A )
timeSeriases = (A{:,4})';   %line
chanSeries = find(strcmp(A{:,3}, 'chan'));
EEG_frequensy = chanSeries(1) - 1;
nChannels = length(chanSeries)+1;
matrix = zeros(EEG_frequensy, nChannels);

matrix(:, 1) = timeSeriases(1:chanSeries(1)-1);
for i = 2 : length(chanSeries)
    matrix(:, i) = timeSeriases(chanSeries(i-1)+1:chanSeries(i)-1); 
end
matrix(:, end) = timeSeriases(chanSeries(end)+1:chanSeries(end)+1+EEG_frequensy-1); 

matrix = matrix';
end

