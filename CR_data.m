function [ CR ] = CR_data( DataMdl, DataOut )
%SR( A, B )
%   
%{ 
%% Size check ------- !!! too slow  !!!
if length(size(DataMdl))~=length(size(DataOut)) || ...
        any(size(DataMdl)-size(DataOut))
    error('Wrong size'); 
end
%}
CR = sum( (DataMdl-DataOut).^2, 1 );
end 